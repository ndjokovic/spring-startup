package com.ey.startup.services.administration.impl;

import com.ey.startup.persistence.administration.entities.Client;
import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.persistence.administration.repositories.ClientRepository;
import com.ey.startup.persistence.administration.repositories.UserRepository;
import com.ey.startup.services.administration.UserService;
import com.ey.startup.util.LocalizedMessageService;
import com.ey.startup.web.administration.mappers.UserMapper;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.UserDto;
import com.ey.startup.web.administration.model.UserListDto;
import com.ey.startup.web.administration.model.UserRegistrationDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final ClientRepository clientRepository;
    private final LocalizedMessageService localizedMessageService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDto findById(final Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return userMapper.fromUserToUserDto(userOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.user.notfound.id", new Object[] {id.toString()}));
        }
    }

    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(userMapper::fromUserToUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public PageDto<UserListDto> getByPage(final Pageable pageable) {
        Page<User> itemsPage = userRepository.findAll(pageable);
        return new PageDto(
                itemsPage.getNumber(),
                itemsPage.getNumberOfElements(),
                itemsPage.getTotalElements(),
                itemsPage.getTotalPages(),
                itemsPage.getContent().stream()
                        .map(userMapper::userToUserListDto)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public UserDto saveNew(final UserDto dto) {
        String encriptedPassword = passwordEncoder.encode(dto.getPassword());
        dto.setPassword(encriptedPassword);
        User savedUser = userRepository.save(userMapper.fromUserDtoToUser(dto));
        return userMapper.fromUserToUserDto(savedUser);
    }

    @Override
    public void update(Long id, UserDto dto) {
        Optional<User> userOptional = userRepository.findById(id);
        userOptional.ifPresentOrElse(user -> {
            userMapper.updateUserEntityWithDto(dto, user);
            userRepository.save(user);
        }, () -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.user.notfound.id", new Object[] {id.toString()}));
        });
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }


    /** User Registration **/
    @Transactional
    @Override
    public void registerUser(UserRegistrationDto dto) {
        Optional<Client> company = clientRepository.findById(dto.getClientId());
        if (company.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.company.notfound.id", new Object[]{dto.getClientId()}));
        }
        String encriptedPassword = passwordEncoder.encode(dto.getPassword());
        dto.setPassword(encriptedPassword);
        User userFromDto = userMapper.fromUserRegistrationDtoToUser(dto);
        userFromDto.setClient(company.get());
        userFromDto.setActive(Boolean.FALSE);
        userRepository.save(userFromDto);
    }


    /** User Authentication **/
    @Override
    public User findByUsername(String username) {
        log.debug("Finding User by username: {}", username);
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (userOptional.isPresent()) {
            log.debug("Username found");
            return userOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.user.notfound.username", new Object[] {username}));
        }
    }


}
