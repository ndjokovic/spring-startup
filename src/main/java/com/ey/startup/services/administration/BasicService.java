package com.ey.startup.services.administration;

import com.ey.startup.web.administration.model.BaseDto;
import com.ey.startup.web.administration.model.PageDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BasicService {

    BaseDto findById(Long id);

    List<BaseDto> findAll();

    PageDto<BaseDto> getByPage(Pageable pageable);

    BaseDto saveNew(BaseDto dto);

    void update(Long id, BaseDto dto);

    void deleteById(Long id);
}
