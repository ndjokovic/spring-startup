package com.ey.startup.services.administration.impl;

import com.ey.startup.persistence.administration.entities.Permission;
import com.ey.startup.persistence.administration.repositories.PermissionRepository;
import com.ey.startup.util.LocalizedMessageService;
import com.ey.startup.web.administration.mappers.PermissionMapper;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.PermissionDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.ey.startup.services.administration.PermissionService;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
@Slf4j
@Service
@AllArgsConstructor
public class PermissionServiceImpl implements PermissionService {

    private final PermissionRepository permissionRepository;
    private final PermissionMapper permissionMapper;
    private final LocalizedMessageService localizedMessageService;

    @Override
    public PermissionDto findById(final Long id) {
        Optional<Permission> permissionOptional = permissionRepository.findById(id);
        if (permissionOptional.isPresent()) {
            return permissionMapper.permissionToPermissionDto(permissionOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.permission.notfound.id", new Object[] {id.toString()}));
        }
    }

    @Override
    public List<PermissionDto> findAll() {
        List<Permission> permissions = permissionRepository.findAll();
        return permissions.stream()
                .map(permissionMapper::permissionToPermissionDto)
                .collect(Collectors.toList());
    }

    @Override
    public PageDto<PermissionDto> getByPage(final Pageable pageable) {
        Page<Permission> itemsPage = permissionRepository.findAll(pageable);
        return new PageDto(
                itemsPage.getNumber(),
                itemsPage.getNumberOfElements(),
                itemsPage.getTotalElements(),
                itemsPage.getTotalPages(),
                itemsPage.getContent().stream()
                        .map(permissionMapper::permissionToPermissionDto)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public PermissionDto saveNew(final PermissionDto dto) {
        return permissionMapper.permissionToPermissionDto(
                permissionRepository.save(
                        permissionMapper.permissionDtoToPermission(dto))
        );
    }

    @Override
    public void update(final Long id, final PermissionDto dto) {
        Optional<Permission> permissionOptional = permissionRepository.findById(id);
        permissionOptional.ifPresentOrElse(permission -> {
            permissionMapper.updateEntityWithDto(dto, permission);
            permissionRepository.save(permission);
        }, () -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.permission.notfound.id", new Object[] {id.toString()}));
        });
    }

    @Override
    public void deleteById(final Long id) {
        permissionRepository.deleteById(id);
    }
}
