package com.ey.startup.services.administration;

import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.RoleDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
public interface RoleService {
    RoleDto findById(Long id);

    List<RoleDto> findAll();

    RoleDto saveNew(RoleDto dto);

    PageDto<RoleDto> getByPage(Pageable pageable);

    void update(Long id, RoleDto dto);

    void deleteById(Long id);
}
