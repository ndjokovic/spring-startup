package com.ey.startup.services.administration;

import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.PermissionDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
public interface PermissionService {

    PermissionDto findById(Long id);

    List<PermissionDto> findAll();

    PageDto<PermissionDto> getByPage(Pageable pageable);

    PermissionDto saveNew(PermissionDto permissionDto);

    void update(Long id, PermissionDto permissionDto);

    void deleteById(Long id);
}
