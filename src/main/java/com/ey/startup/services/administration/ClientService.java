package com.ey.startup.services.administration;

import com.ey.startup.persistence.administration.entities.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ClientService {
    Optional<Client> findById(final Long id);

    List<Client> findAll();

    Page<Client> getByPage(final Pageable pageable);

    Client saveNew(final Client client);

    void update(final Client client);

    void deleteById(final Long id);
}
