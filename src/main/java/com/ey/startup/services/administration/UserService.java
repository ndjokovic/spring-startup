package com.ey.startup.services.administration;

import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.UserDto;
import com.ey.startup.web.administration.model.UserListDto;
import com.ey.startup.web.administration.model.UserRegistrationDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
public interface UserService {
    UserDto findById(Long id);

    List<UserDto> findAll();

    UserDto saveNew(UserDto dto);

    PageDto<UserListDto> getByPage(Pageable pageable);

    void update(Long id, UserDto dto);

    void deleteById(Long id);

    User findByUsername(String username);

    void registerUser(UserRegistrationDto dto);
}
