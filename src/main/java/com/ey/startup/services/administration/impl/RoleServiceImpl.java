package com.ey.startup.services.administration.impl;

import com.ey.startup.persistence.administration.entities.Role;
import com.ey.startup.persistence.administration.repositories.RoleRepository;
import com.ey.startup.services.administration.RoleService;
import com.ey.startup.util.LocalizedMessageService;
import com.ey.startup.web.administration.mappers.RoleMapper;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.RoleDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */

@Slf4j
@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;
    private final LocalizedMessageService localizedMessageService;

    @Override
    public RoleDto findById(final Long id) {
        Optional<Role> roleOptional = roleRepository.findById(id);
        if (roleOptional.isPresent()) {
            return roleMapper.fromRoleToRoleDto(roleOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.role.notfound.id", new Object[] {id.toString()}));
        }
    }

    @Override
    public List<RoleDto> findAll() {
        List<Role> roles = roleRepository.findAll();
        return roles.stream()
                .map(roleMapper::fromRoleToRoleDtoFlat)
                .collect(Collectors.toList());
    }

    @Override
    public PageDto<RoleDto> getByPage(Pageable pageable) {
        Page<Role> itemsPage = roleRepository.findAll(pageable);
        return new PageDto(
                itemsPage.getNumber(),
                itemsPage.getNumberOfElements(),
                itemsPage.getTotalElements(),
                itemsPage.getTotalPages(),
                itemsPage.getContent().stream()
                        .map(roleMapper::fromRoleToRoleDtoFlat)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public RoleDto saveNew(final RoleDto dto) {
        return roleMapper.fromRoleToRoleDto(
                roleRepository.save(roleMapper.fromRoleDtoToRole(dto)));

    }

    @Override
    public void update(final Long id, final RoleDto dto) {
        Optional<Role> roleOptional = roleRepository.findById(id);
        roleOptional.ifPresentOrElse(role -> {
            roleMapper.updateEntityWithDto(dto, role);
            roleRepository.save(role);
        }, () -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.role.notfound.id", new Object[] {id.toString()}));
        });
    }

    @Override
    public void deleteById(final Long id) {
        roleRepository.deleteById(id);
    }
}
