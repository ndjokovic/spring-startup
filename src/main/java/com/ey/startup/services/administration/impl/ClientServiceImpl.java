package com.ey.startup.services.administration.impl;

import com.ey.startup.persistence.administration.entities.Client;
import com.ey.startup.persistence.administration.repositories.ClientRepository;
import com.ey.startup.services.administration.ClientService;
import com.ey.startup.util.LocalizedMessageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final LocalizedMessageService localizedMessageService;

    public ClientServiceImpl(final ClientRepository clientRepository,
                             final LocalizedMessageService localizedMessageService) {
        this.clientRepository = clientRepository;
        this.localizedMessageService = localizedMessageService;
    }

    @Override
    public Optional<Client> findById(final Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public List<Client> findAll() {
        return Collections.unmodifiableList(clientRepository.findAll());
    }

    @Override
    public Page<Client> getByPage(final Pageable pageable) {
        return clientRepository.findAll(pageable);
    }

    @Override
    public Client saveNew(final Client client) {
        return clientRepository.saveAndFlush(client);
    }

    @Override
    public void update(final Client client) {
        if (client.getId() != null && clientRepository.existsById(client.getId())) {
            clientRepository.save(client);
        }
    }

    @Override
    public void deleteById(final Long id) {
        Optional<Client> companyToDelete = clientRepository.findById(id);
        companyToDelete.ifPresentOrElse(clientRepository::delete, () -> {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, localizedMessageService.getLocalizedMessage("error.person.notfound.id", new Object[]{id.toString()}));
        });
    }
}
