package com.ey.startup.web.validation;

import com.ey.startup.util.LocalizedMessageService;
import lombok.extern.slf4j.Slf4j;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/15/2020
 */
@Slf4j
public class ConditionalValidator implements ConstraintValidator<Conditional, Object> {

    private String message;
    private String selected;
    private String[] required;
    private String[] messages;
    private String[] values;

    @Autowired
    LocalizedMessageService localizedMessageService;

    @Override
    public void initialize(Conditional requiredIfChecked) {
        selected = requiredIfChecked.selected();
        required = requiredIfChecked.required();
        messages = requiredIfChecked.messages();
        values = requiredIfChecked.values();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        Boolean allConditionsValid = true;

        try {
            Object checkedValue = BeanUtils.getProperty(object, selected);
            if (Arrays.asList(values).contains(checkedValue)) {
                int messagesCounter = 0;
                for (String propName : required) {
                    Boolean valid = true;
                    Object requiredValue = BeanUtils.getProperty(object, propName);
                    valid = requiredValue != null && StringUtils.hasText((String)requiredValue);
                    if (!valid) {
                        if (allConditionsValid) allConditionsValid = false;
                        context.disableDefaultConstraintViolation();
                        String localizedMessage = localizedMessageService.getLocalizedMessageNoArgs(messages[messagesCounter]);
                        context.buildConstraintViolationWithTemplate(localizedMessage).addPropertyNode(propName).addConstraintViolation();
                    }
                    messagesCounter = messagesCounter + 1;
                }
            }
        } catch (IllegalAccessException e) {
            log.error("Accessor method is not available for class : {}, exception : {}", object.getClass().getName(), e);
            e.printStackTrace();
            return false;
        } catch (NoSuchMethodException e) {
            log.error("Field or method is not present on class : {}, exception : {}", object.getClass().getName(), e);
            e.printStackTrace();
            return false;
        } catch (InvocationTargetException e) {
            log.error("An exception occurred while accessing class : {}, exception : {}", object.getClass().getName(), e);
            e.printStackTrace();
            return false;
        }
        return allConditionsValid;
    }


}