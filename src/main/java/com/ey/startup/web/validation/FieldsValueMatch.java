package com.ey.startup.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/15/2020
 */
/*
@Constraint(validatedBy = FieldsValueMatchValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)


@Repeatable(Conditionals.class)
*/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {FieldsValueMatchValidator.class})
public @interface FieldsValueMatch {

    String message() default "Fields values don't match!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field();

    String fieldMatch();

    @Target({ ElementType.TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        FieldsValueMatch[] value();
    }

}
