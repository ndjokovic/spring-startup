package com.ey.startup.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */

@Target({ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PasswordConstraintValidator.class})
public @interface ValidPassword {

    String message() default "Invalid Password format.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};


}
