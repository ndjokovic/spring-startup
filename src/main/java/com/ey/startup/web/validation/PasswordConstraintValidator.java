package com.ey.startup.web.validation;

import com.ey.startup.util.LocalizedMessageService;
import com.google.common.base.Joiner;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */
@Slf4j
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Autowired
    LocalizedMessageService localizedMessageService;

    @Override
    public void initialize(final ValidPassword arg0) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 30),
                new UppercaseCharacterRule(1),
                new DigitCharacterRule(1),
                new SpecialCharacterRule(1),
                //new NumericalSequenceRule(3,false),
                //new AlphabeticalSequenceRule(3,false),
                //new QwertySequenceRule(3,false),
                new WhitespaceRule()));

        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }

        String localizedMessage = localizedMessageService.getLocalizedMessageNoArgs("validation.user.password.invalidformat");
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(localizedMessage).addConstraintViolation();
        return false;
    }


}
