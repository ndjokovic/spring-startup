package com.ey.startup.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
@Slf4j
public class BaseController {

    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 10;
    private static final String DEFAULT_SORT_BY = "id";

    public Pageable createPageRequest(String pageNumberStr, String pageSizeStr, String sortBy, String sortDirection) {

        Integer pageNumber = DEFAULT_PAGE_NUMBER;
        Integer pageSize = DEFAULT_PAGE_SIZE;
        Sort.Direction direction = Sort.Direction.ASC;

        if (StringUtils.hasText(pageNumberStr) && Integer.parseInt(pageNumberStr) >= 0) {
            pageNumber = Integer.parseInt(pageNumberStr);
        }

        if (StringUtils.hasText(pageSizeStr) && Integer.parseInt(pageSizeStr) > 0) {
            pageSize = Integer.parseInt(pageSizeStr);
        }

        if (!StringUtils.hasText(sortBy)) {
            sortBy = DEFAULT_SORT_BY;
        }

        if (StringUtils.hasText(sortDirection) && sortDirection.equalsIgnoreCase("desc")) {
            direction = Sort.Direction.DESC;
        }

        log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.debug("+++ pageNumber: " + pageNumber);
        log.debug("+++ pageSize: " + pageSize);
        log.debug("+++ sortBy: " + sortBy);
        log.debug("+++ sortDirection: " + direction);

        return PageRequest.of(pageNumber, pageSize, direction, sortBy);

    }

}
