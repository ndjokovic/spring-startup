package com.ey.startup.web.administration.mappers;

import com.ey.startup.persistence.administration.entities.Client;
import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.web.administration.model.ClientDto;
import com.ey.startup.web.administration.model.UserDto;
import org.mapstruct.*;

import java.util.Set;

@Mapper
public interface ClientMapper {

    @Mapping( target = "users", qualifiedByName = {"usersToFlatDtos"})
    ClientDto clientToClientDto(final Client client);

    @Named(value = "usersToFlatDtos")
    @Mapping(target = "verifyPassword", ignore = true)
    Set<UserDto> usersToFlatDtos(Set<User> entities);


    Client clientDtoToClient(ClientDto dto);

    void fromDtoToExistingEntity(ClientDto dto, @MappingTarget Client client);

}
