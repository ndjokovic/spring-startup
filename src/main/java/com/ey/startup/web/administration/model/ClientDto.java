package com.ey.startup.web.administration.model;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClientDto extends BaseDto {
    private String idNumber;
    private String taxNumber;
    private String name;
    private String address;
    private String phoneNumber;
    private Set<UserDto> users = new HashSet<>();
}
