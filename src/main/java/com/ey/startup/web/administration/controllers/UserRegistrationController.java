package com.ey.startup.web.administration.controllers;

import com.ey.startup.services.administration.UserService;
import com.ey.startup.web.administration.model.UserRegistrationDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/registerUser")
@RestController
public class UserRegistrationController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity registerUser(@RequestBody  @Valid UserRegistrationDto dto) {
        log.debug("+++++++++ User registration input +++++++++");
        log.debug(dto.toString());
        userService.registerUser(dto);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
