package com.ey.startup.web.administration.controllers;

import com.ey.startup.services.administration.PermissionService;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.PermissionDto;
import com.ey.startup.web.controllers.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
@Api( tags = "Application permissions")
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/permission")
@RestController
public class PermissionController extends BaseController {

    private final int DEFAULT_PAGE_NUMBER = 0;
    private final int DEFAULT_PAGE_SIZE = 3;

    private final PermissionService permissionService;

    @ApiOperation(value = "Return permission for specific ID.")
    @GetMapping({"{id}"})
    //@HasPermission.PermissionRead
    public ResponseEntity getById(@PathVariable("id") Long id) {
        PermissionDto dto = permissionService.findById(id);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @ApiOperation(value = "Return all permission.")
    @GetMapping(path = "all")
    //@HasPermission.PermissionRead
    public ResponseEntity getAll() {
        List<PermissionDto> permissions = permissionService.findAll();
        return new ResponseEntity(permissions, HttpStatus.OK);
    }

    //@HasPermission.PermissionRead
    @GetMapping(path = "byPage", produces = { "application/json" })
    public ResponseEntity<PageDto> getByPage(
                @RequestParam(value = "pageNumber", required = false) String pageNumber,
                @RequestParam(value = "pageSize", required = false) String pageSize,
                @RequestParam(value = "sortBy", required = false) String sortBy,
                @RequestParam(value = "sortDirection", required = false) String sortDirection) {

        PageDto<PermissionDto> page = permissionService.getByPage(createPageRequest(pageNumber, pageSize, sortBy, sortDirection));
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @ApiOperation(value = "Create new permission.")
    @PostMapping
    //@HasPermission.PermissionWrite
    public ResponseEntity saveNew(@RequestBody @Valid PermissionDto dto){
        PermissionDto savedDto = permissionService.saveNew(dto);
        return new ResponseEntity(savedDto, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update existing permission for provided ID.")
    @PutMapping(path = {"{id}"}, produces = { "application/json" })
    //@HasPermission.PermissionWrite
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody @Valid PermissionDto dto){
        permissionService.update(id, dto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "Delete permission for provided ID.")
    @DeleteMapping({"{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@HasPermission.PermissionDelete
    public void delete(@PathVariable("id") Long id){
        permissionService.deleteById(id);
    }



}
