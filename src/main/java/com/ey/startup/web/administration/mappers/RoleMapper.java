package com.ey.startup.web.administration.mappers;

import com.ey.startup.persistence.administration.entities.Permission;
import com.ey.startup.persistence.administration.entities.Role;
import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.web.administration.model.PermissionDto;
import com.ey.startup.web.administration.model.RoleDto;
import com.ey.startup.web.administration.model.UserDto;
import org.mapstruct.*;

import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/14/2020
 */
@Mapper (uses = {PermissionMapper.class})
public interface RoleMapper {

    @Mapping(target = "users", ignore = true)
    public RoleDto fromRoleToRoleDto(Role entity);

    @Mapping(target = "users", ignore = true)
    public Role fromRoleDtoToRole(RoleDto dto);

    @Mapping(target = "users", ignore = true)
    @Mapping(target = "permissions", ignore = true)
    public RoleDto fromRoleToRoleDtoFlat(Role entity);

    void updateEntityWithDto(RoleDto dto, @MappingTarget Role entity);

}
