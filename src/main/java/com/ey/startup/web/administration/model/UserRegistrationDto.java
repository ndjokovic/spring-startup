package com.ey.startup.web.administration.model;

import com.ey.startup.web.validation.Conditional;
import com.ey.startup.web.validation.FieldsValueMatch;
import com.ey.startup.web.validation.ValidPassword;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldsValueMatch( field = "password", fieldMatch = "verifyPassword", message = "{validation.user.password.notMatch}")
public class UserRegistrationDto {

    @NotBlank(message = "{validation.user.username.notempty}")
    private String username;
    @NotBlank(message = "{validation.user.password.notempty}")
    @ValidPassword
    private String password;
    @NotBlank(message = "{validation.user.verifyPassword.notempty}")
    private String verifyPassword;

    private String firstName;
    private String lastName;
    @NotEmpty(message = "{validation.company.taxnumber.notempty")
    private Long clientId;

    @Email(message = "{validation.user.email.invalid}")
    @NotBlank(message = "{validation.user.email.notempty}")
    private String email;

}
