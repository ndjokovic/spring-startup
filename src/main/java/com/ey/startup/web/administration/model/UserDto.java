package com.ey.startup.web.administration.model;

import com.ey.startup.web.validation.FieldsValueMatch;
import com.ey.startup.web.validation.ValidPassword;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@FieldsValueMatch( field = "password", fieldMatch = "verifyPassword", message = "{validation.user.password.notMatch}")
public class UserDto  extends BaseDto {

    @NotBlank(message = "{validation.user.namename.notempty}")
    private String username;
    private Boolean active = Boolean.FALSE;
    @ValidPassword
    private String password;
    private String verifyPassword;
    private String firstName;
    private String lastName;
    @Email
    private String email;
    private Set<RoleDto> roles;
    private ClientDto client;

}
