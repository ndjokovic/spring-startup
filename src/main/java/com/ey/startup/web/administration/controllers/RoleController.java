package com.ey.startup.web.administration.controllers;

import com.ey.startup.services.administration.RoleService;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.RoleDto;
import com.ey.startup.web.controllers.BaseController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/role")
@RestController
public class RoleController extends BaseController {

    private final RoleService roleService;

    @GetMapping({"{id}"})
    //@HasPermission.RoleRead
    public ResponseEntity getById(@PathVariable("id") Long id) {
        RoleDto dto = roleService.findById(id);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @GetMapping(path = "all")
    //@HasPermission.RoleRead
    public ResponseEntity getAll() {
        List<RoleDto> roles = roleService.findAll();
        return new ResponseEntity(roles, HttpStatus.OK);
    }

    @GetMapping(path = "byPage", produces = { "application/json" })
    //@HasPermission.RoleRead
    public ResponseEntity<PageDto> getByPage(
            @RequestParam(value = "pageNumber", required = false) String pageNumber,
            @RequestParam(value = "pageSize", required = false) String pageSize,
            @RequestParam(value = "sortBy", required = false) String sortBy,
            @RequestParam(value = "sortDirection", required = false) String sortDirection) {

        PageDto<RoleDto> page = roleService.getByPage(createPageRequest(pageNumber, pageSize, sortBy, sortDirection));
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @PostMapping
    //@HasPermission.RoleWrite
    public ResponseEntity saveNew(@RequestBody @Valid RoleDto dto) {
        RoleDto savedDto = roleService.saveNew(dto);
        return new ResponseEntity(savedDto, HttpStatus.CREATED);
    }

    @PutMapping(path = {"{id}"}, produces = { "application/json" })
    //@HasPermission.RoleWrite
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody @Valid RoleDto dto){
        //RoleDto updatedDto =
        roleService.update(id, dto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping({"{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    //@HasPermission.RoleDelete
    public void delete(@PathVariable("id") Long id){
        roleService.deleteById(id);
    }
}
