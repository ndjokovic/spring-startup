package com.ey.startup.web.administration.mappers;

import com.ey.startup.persistence.administration.entities.Permission;
import com.ey.startup.web.administration.model.PermissionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/14/2020s
 */
@Mapper
public interface PermissionMapper {

    @Mapping(target = "roles", ignore = true)
    PermissionDto permissionToPermissionDto(Permission entity);

    Permission permissionDtoToPermission(PermissionDto dto);

    @Mapping(target = "roles", ignore = true)
    Set<PermissionDto> fromEntitiesToDtos(Set<Permission> entities);

    @Mapping(target = "roles", ignore = true)
    Set<Permission> fromDtosToEntities(Set<PermissionDto> entities);

    void updateEntityWithDto(PermissionDto dto, @MappingTarget Permission entity);

}
