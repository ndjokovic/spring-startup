package com.ey.startup.web.administration.model;

import lombok.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PermissionDto extends BaseDto {

    @NotBlank(message = "{validation.permission.name.notempty}")
    private String name;

    private String description;
    private Set<RoleDto> roles;

}
