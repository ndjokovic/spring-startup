package com.ey.startup.web.administration.model;

import com.ey.startup.persistence.administration.entities.Permission;
import com.ey.startup.persistence.administration.entities.User;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RoleDto extends BaseDto {

    @NotBlank(message = "{validation.role.name.notempty}")
    private String name;
    private String description;
    private Set<UserDto> users;
    private Set<PermissionDto> permissions;
}
