package com.ey.startup.web.administration.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserListDto extends BaseDto {
    private String username;
    private String firstName;
    private String lastName;
    private String clientName;
    private String email;
}
