package com.ey.startup.web.administration.controllers;

import com.ey.startup.persistence.administration.entities.Client;
import com.ey.startup.services.administration.ClientService;
import com.ey.startup.web.administration.mappers.ClientMapper;
import com.ey.startup.web.administration.model.ClientDto;
import com.ey.startup.web.controllers.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/company")
public class ClientController extends BaseController {

    private final ClientService clientService;
    private final ClientMapper clientMapper;

    public ClientController(ClientService clientService, ClientMapper clientMapper) {
        this.clientService = clientService;
        this.clientMapper = clientMapper;
    }

    @GetMapping("{id}")
    public ResponseEntity getById(@PathVariable Long id) {
        log.debug("Start method getById, param: id={}", id);
        Optional<Client> company = clientService.findById(id);
        ResponseEntity response = new ResponseEntity<>(HttpStatus.OK);
        if (company.isPresent()) {
            ClientDto dto = clientMapper.clientToClientDto(company.get());
            response = new ResponseEntity<>(dto, HttpStatus.OK);
        }
        log.debug("End method getById, company exist=" + company.isPresent());
        return response;
    }

    @GetMapping("all")
    public ResponseEntity getAll() {
        log.debug("Start method getAll");
        List<ClientDto> companies = clientService.findAll()
                .stream().map(clientMapper::clientToClientDto)
                .collect(Collectors.toList());
        Map<String, Object> response = new HashMap<>();
        response.put("companies", companies);
        log.debug("End method getAll");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveNew(@RequestBody @Valid ClientDto dto) {
        log.debug("Start method saveNew");
        clientService.saveNew(clientMapper.clientDtoToClient(dto));
        log.debug("End method saveNew");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(path = {"{id}"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable Long id, @RequestBody @Valid ClientDto clientDto) {
        log.debug("Start method update, param id={}", id);
        if (clientDto != null) {
            clientService.update(clientMapper.clientDtoToClient(clientDto));
        }
        log.debug("End method update, param id={}", id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable final Long id) {
        log.debug("Start method delete, param id={}", id);
        if (id != null) {
            clientService.deleteById(id);
        }
        log.debug("End method delete, param id={}", id);
    }


}
