package com.ey.startup.web.administration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseDto {
    private Long id = null;
    private OffsetDateTime createdDate;
    private OffsetDateTime lastModifiedDate;
    private String createdBy;
    private String modifiedBy;
}
