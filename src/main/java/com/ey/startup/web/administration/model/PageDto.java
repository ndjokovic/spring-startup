package com.ey.startup.web.administration.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/19/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageDto<T> {
    private int currentPage;
    private int elementsOnPage;
    private long totalElements;
    private int totalPages;

    private List<T> content = new ArrayList<T>();
}
