package com.ey.startup.web.administration.mappers;

import com.ey.startup.persistence.administration.entities.Client;
import com.ey.startup.persistence.administration.entities.Role;
import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.web.administration.model.*;
import org.mapstruct.*;

import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/14/2020
 */
@Mapper
public interface UserMapper {

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "verifyPassword", ignore = true)
    @Mapping( target = "roles", qualifiedByName = { "rolesToFlatDtos"} )
    @Mapping(target = "client", source = "client")
    public UserDto fromUserToUserDto(User entity);

    @Mapping(target = "users", ignore = true)
    @Mapping(target = "permissions", ignore = true)
    public RoleDto roleToFlatDto(Role role);

    @Named(value = "rolesToFlatDtos")
    Set<RoleDto> rolesToFlatDtos(Set<Role> entities);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "idNumber", source = "idNumber")
    @Mapping(target = "taxNumber", source = "taxNumber")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "address", source = "address")
    @Mapping(target = "phoneNumber", source = "phoneNumber")
    @Mapping(target = "users", ignore = true)
    ClientDto clientToClientDto(final Client client);

    //@Mapping(target = "passwordVerify", ignore = true)
    @Mapping(target = "email", source = "dto.email")
    public User fromUserDtoToUser(UserDto dto);

    @Mapping(target = "email", source = "email")
    @Mapping(target = "firstName", source = "firstName")
    @Mapping(target = "lastName", source = "lastName")
    void updateUserEntityWithDto(UserDto dto, @MappingTarget User entity);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target="id", source="id")
    @Mapping(target="createdDate", source="createdDate")
    @Mapping(target="lastModifiedDate", source="lastModifiedDate")
    @Mapping(target="createdBy", source="createdBy")
    @Mapping(target="modifiedBy", source="modifiedBy")
    @Mapping(target="username", source="username")
    @Mapping(target="firstName", source="firstName")
    @Mapping(target="lastName", source="lastName")
    @Mapping(target="clientName", source="client.name")
    @Mapping(target="email", source="email")
    public UserListDto userToUserListDto(User user);


    @BeanMapping(ignoreByDefault = true)
    @Mapping(target="username", source="username")
    @Mapping(target="password", source="password")
    //@Mapping(target="verifyPassword", source="verifyPassword")
    public User fromUserRegistrationDtoToUser(UserRegistrationDto dto);

}
