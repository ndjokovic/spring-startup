package com.ey.startup.web.administration.controllers;

import com.ey.startup.security.permissions.HasPermission;
import com.ey.startup.services.administration.UserService;
import com.ey.startup.web.administration.model.PageDto;
import com.ey.startup.web.administration.model.RoleDto;
import com.ey.startup.web.administration.model.UserDto;
import com.ey.startup.web.administration.model.UserListDto;
import com.ey.startup.web.controllers.BaseController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/user")
@RestController
public class UserController extends BaseController {

    private final UserService userService;

    @GetMapping({"{id}"})
    //@HasPermission.UserRead
    public ResponseEntity getById(@PathVariable("id") Long id) {
        UserDto dto = userService.findById(id);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @GetMapping(path = "all")
    //@HasPermission.UserRead
    public ResponseEntity getAll() {
        List<UserDto> roles = userService.findAll();
        return new ResponseEntity(roles, HttpStatus.OK);
    }

    @GetMapping(path = "byPage", produces = { "application/json" })
    //@HasPermission.UserRead
    public ResponseEntity<PageDto> getByPage(
            @RequestParam(value = "pageNumber", required = false) String pageNumber,
            @RequestParam(value = "pageSize", required = false) String pageSize,
            @RequestParam(value = "sortBy", required = false) String sortBy,
            @RequestParam(value = "sortDirection", required = false) String sortDirection) {

        PageDto<UserListDto> page = userService.getByPage(createPageRequest(pageNumber, pageSize, sortBy, sortDirection));
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @PostMapping
    //@HasPermission.UserWrite
    public ResponseEntity saveNew(@RequestBody @Valid UserDto dto){
        UserDto savedDto = userService.saveNew(dto);
        return new ResponseEntity(savedDto, HttpStatus.CREATED);
    }

    @PutMapping(path = {"{id}"}, produces = { "application/json" })
    //@HasPermission.UserWrite
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody @Valid UserDto dto){
        log.debug("*********** NoOfRoles: " + dto.getRoles().size());
        userService.update(id, dto);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping({"{id}"})
    @ResponseStatus(HttpStatus.NO_CONTENT)
   // @HasPermission.UserDelete
    public void delete(@PathVariable("id") Long id){
        userService.deleteById(id);
    }


}
