package com.ey.startup.web.errorhandler;

import com.ey.startup.util.LocalizedMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/17/2020
 */

@Slf4j
@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    private final LocalizedMessageService localizedMessageService;

    @Autowired
    public CustomRestExceptionHandler(LocalizedMessageService localizedMessageService) {
        this.localizedMessageService = localizedMessageService;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.debug("++++++++++++ HANDLE: MethodArgumentNotValidException");
        List<String> errors = new ArrayList<String>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            errors.add(error.getDefaultMessage());
        });
        ApiError apiError = new ApiError(
                HttpStatus.BAD_REQUEST,
                HttpStatus.BAD_REQUEST.value(),
                localizedMessageService.getLocalizedMessageNoArgs("validation.general.methodargumentmessage"),
                errors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @ExceptionHandler({ DataIntegrityViolationException.class })
    public ResponseEntity<Object> handleDataIntegrity(final DataIntegrityViolationException ex, final WebRequest request) {
        log.debug("++++++++++++ HANDLE: DataIntegrityViolationException");
        String rootMessage = ex.getRootCause().getMessage();  // TODO: delete after testing
        log.debug("++++++++++++ ROOT CAUSE: " + rootMessage); // TODO: delete after testing

        final ApiError apiError = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                localizedMessageService.getLocalizedMessageNoArgs("error.server.dataintegrity.violated"),
                Arrays.asList(ex.getRootCause().getMessage()));
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ ResponseStatusException.class })
    public ResponseEntity<Object> handleResponseStatus(final ResponseStatusException ex, final WebRequest request) {
        log.debug("++++++++++++ HANDLE: ResponseStatusException");
        final ApiError apiError = new ApiError(
                ex.getStatus(),
                ex.getStatus().value(),
                localizedMessageService.getLocalizedMessageNoArgs("error.request.general"),
                Arrays.asList(ex.getReason()));
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }


    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        log.debug("++++++++++++ HANDLE: Exception");
        ex.printStackTrace();
        final ApiError apiError = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                localizedMessageService.getLocalizedMessageNoArgs("validation.general.servererror"),
                null);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

}
