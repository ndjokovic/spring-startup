package com.ey.startup.web.errorhandler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/17/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

    private HttpStatus status;
    private int statusCode;
    private String message;
    private List<String> errors;
/*
    public ApiError(final HttpStatus status, final int statusCode, final String message, final String error) {
        this.status = status;
        this.statusCode = statusCode;
        this.message = message;
        errors = Arrays.asList(error);
    }
*/
}
