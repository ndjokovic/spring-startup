package com.ey.startup.config;

import com.ey.startup.util.AuditorAwareImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/12/2020
 */

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories(basePackages="com.ey.startup")
public class PersistenceConfig {
    @Bean
    public AuditorAware<String> auditorProvider() {
        return new AuditorAwareImpl();
    }
}
