package com.ey.startup.util;

import com.ey.startup.security.model.ApplicationUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;
import java.util.Optional;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/12/2020
 */
@Slf4j
public class AuditorAwareImpl  implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
        //ApplicationUser appUser = (ApplicationUser)authentication.getPrincipal();
        String principal = (String)authentication.getPrincipal();
        log.debug("-------- User: " + principal);
        return Optional.of(principal);
        //return Optional.of(appUser.getUsername());
    }
}
