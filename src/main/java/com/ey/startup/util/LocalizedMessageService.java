package com.ey.startup.util;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/15/2020
 */
@AllArgsConstructor
@Component
public  class LocalizedMessageService {

    private MessageSource messageSource;

    public String getLocalizedMessage(String messageCode, Object[] args) {
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(messageCode,  args, locale);
        return message;
    }

    public String getLocalizedMessageNoArgs(String messageCode) {
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(messageCode,  null, locale);
        return message;
    }
}
