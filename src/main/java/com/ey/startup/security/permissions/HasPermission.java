package com.ey.startup.security.permissions;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/10/2020
 */
public class HasPermission {

    /** CLIENT **/
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('client.read')") public @interface ClientRead {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('client.write')") public @interface ClientWrite {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('client.delete')") public @interface ClientDelete {}

    /** USER **/
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('user.read')") public @interface UserRead {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('user.write')") public @interface UserWrite {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('user.delete')") public @interface UserDelete {}

    /** ROLE **/
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('role.read')") public @interface RoleRead {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('role.write')") public @interface RoleWrite {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('role.delete')") public @interface RoleDelete {}

    /** PERMISSION **/
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('permission.read')") public @interface PermissionRead {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('permission.write')") public @interface PermissionWrite {}
    @Retention(RetentionPolicy.RUNTIME) @PreAuthorize("hasAuthority('permission.delete')") public @interface PermissionDelete {}



}
