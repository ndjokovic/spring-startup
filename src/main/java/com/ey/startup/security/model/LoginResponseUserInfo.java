package com.ey.startup.security.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/10/2020
 */
@Getter
@Setter
public class LoginResponseUserInfo {

    private String username;
    private Set<String> permissions;

}
