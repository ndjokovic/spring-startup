package com.ey.startup.security.services;

import com.ey.startup.persistence.administration.entities.User;
import com.ey.startup.security.model.ApplicationUser;
import com.ey.startup.services.administration.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/9/2020
 */
@Service
public class ApplicationUserService implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public ApplicationUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByUsername(username);
        return ApplicationUser.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRoles())
                .build();
    }
}
