package com.ey.startup.persistence.administration.entities;

import com.ey.startup.persistence.BaseEntity;
import lombok.*;
import org.checkerframework.common.aliasing.qual.Unique;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user",  indexes={@Index(columnList="username")})
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "user_id_sequence", allocationSize = 1)
public class User extends BaseEntity {

    @Unique
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Boolean active;
    private String email;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "client_id", foreignKey = @ForeignKey(name = "client_user_fk"))
    private Client client;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")})
    private Set<Role> roles;

    @Builder
    public User(Long id, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate,
                String createdBy, String modifiedBy, String username, Boolean active,
                String password, String email, Client client, Set<Role> roles) {
        super(id, createdDate, lastModifiedDate, createdBy, modifiedBy);
        this.username = username;
        this.password = password;
        this.client = client;
        this.active = active;
        this.email = email;
        this.roles = roles;
    }

}
