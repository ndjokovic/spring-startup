package com.ey.startup.persistence.administration.repositories;

import com.ey.startup.persistence.administration.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
