package com.ey.startup.persistence.administration.entities;

import com.ey.startup.persistence.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "client")
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(name = "idgen", sequenceName = "client_id_sequence", allocationSize = 1)
public class Client extends BaseEntity {

    private String idNumber;
    private String taxNumber;
    private String name;
    private String address;
    private String phoneNumber;

    @OneToMany(mappedBy="client")
    private Set<User> users = new HashSet<>();

    @Builder
    public Client(Long id, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, String createdBy, String modifiedBy, String idNumber, String taxNumber, String name, String address, String phoneNumber, Set<User> users) {
        super(id, createdDate, lastModifiedDate, createdBy, modifiedBy);
        this.idNumber = idNumber;
        this.taxNumber = taxNumber;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.users = users;
    }
}
