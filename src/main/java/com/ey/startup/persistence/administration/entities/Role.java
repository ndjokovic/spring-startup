package com.ey.startup.persistence.administration.entities;

import com.ey.startup.persistence.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "role")
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "role_id_sequence", allocationSize = 1)
public class Role extends BaseEntity {

    private String name;
    private String description;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission",
            joinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "PERMISSION_ID", referencedColumnName = "ID")})
    private Set<Permission> permissions;

    public Role(Long id, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate, String createdBy,
                String modifiedBy, String name, String description, Set<User> users, Set<Permission> permissions) {
        super(id, createdDate, lastModifiedDate, createdBy, modifiedBy);
        this.name = name;
        this.description = description;
        this.users = users;
        this.permissions = permissions;
    }

}
