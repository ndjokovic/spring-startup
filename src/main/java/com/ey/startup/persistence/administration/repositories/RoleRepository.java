package com.ey.startup.persistence.administration.repositories;

import com.ey.startup.persistence.administration.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
}
