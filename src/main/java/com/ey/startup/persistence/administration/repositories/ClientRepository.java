package com.ey.startup.persistence.administration.repositories;

import com.ey.startup.persistence.administration.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByTaxNumber(final String taxNumber);
}
