package com.ey.startup.persistence.administration.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ey.startup.persistence.administration.entities.Permission;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */
public interface PermissionRepository extends JpaRepository<Permission, Long> {
}
