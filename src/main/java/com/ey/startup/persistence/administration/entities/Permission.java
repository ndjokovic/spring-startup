package com.ey.startup.persistence.administration.entities;

import com.ey.startup.persistence.BaseEntity;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.Set;

/**
 * @author Nenad Djokovic
 * @version 1.0
 * @date 12/8/2020
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "permission")
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "permission_id_sequence", allocationSize = 1)
public class Permission extends BaseEntity {

    private String name;
    private String description;

    @ManyToMany(mappedBy = "permissions")
    private Set<Role> roles;

    public Permission(Long id, OffsetDateTime createdDate, OffsetDateTime lastModifiedDate,
                      String createdBy, String modifiedBy, String name, String description, Set<Role> roles) {
        super(id, createdDate, lastModifiedDate, createdBy, modifiedBy);
        this.name = name;
        this.description = description;
        this.roles = roles;
    }


}
