# Permissions
INSERT INTO
    permission (id, created_by, created_date, last_modified_date, modified_by, description, name)
VALUES
    (1, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'User permission to read the data', 'user.read'),
    (2, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'User permission to change the data', 'user.write'),
    (3, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'User permission to remove the data', 'user.delete'),
    (4, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Client permission to read the data', 'client.read'),
    (5, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Client permission to change the data', 'client.write'),
    (6, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Client permission to remove the data', 'client.delete'),
    (7, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'This role has permission to read the data', 'role.read'),
    (8, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'This role has permission to change the data', 'role.write'),
    (9, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'This role has permission to remove the data', 'role.delete')
;

# Roles
INSERT INTO
    role (id, created_by, created_date, last_modified_date, modified_by, description, name)
VALUES
    (1, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Administrator who has full control of content on website', 'super admin'),
    (2, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Regular employee on client level', 'employee'),
    (3, 'anonymousUser', CURRENT_DATE, NULL, NULL, 'Administrator on client level', 'client admin')
;
# Permission-Role relation
INSERT INTO
    role_permission (role_id, permission_id)
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (2, 1),
    (2, 2),
    (2, 3),
    (3, 4),
    (3, 5),
    (3, 6)
;
# Client
# INSERT INTO
INSERT INTO
    client (id, created_by, created_date, last_modified_date, modified_by, address, id_number, name, phone_number, tax_number)
VALUES
    (1, 'admin', CURRENT_DATE, NULL, NULL, 'Bulevar Mihajla Pupina 115-D, 11000 Beograd', 17155270, 'EY DOO', '(011) 2095-800', 101824091),
    (2, 'admin', CURRENT_DATE, NULL, NULL, 'Industrijsko naselje bb, Zrenjaninski put, 11213 Padinska Skela', 07042701, 'Imlek DOO', '(011) 305-05-05', 100001636)
;
# User with client
INSERT INTO
    user (id, created_by, created_date, last_modified_date, modified_by, active, email, first_name, last_name, password, username, client_id)
VALUES
    (1, 'admin', CURRENT_DATE, NULL, NULL, true, 'damjan.zamaklar@rs.ey.com', 'Damjan', 'Zamaklar', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'dzamaklar', 1),
    (2, 'admin', CURRENT_DATE, NULL, NULL, true, 'nenad.djokovic@rs.ey.com', 'Nenad', 'Djokovic', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'ndjokovic', 1),
    (3, 'admin', CURRENT_DATE, NULL, NULL, true, 'srdjan.djokic@rs.ey.com', 'Srdjan', 'Djokic', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'sdjokic', 1),
    (4, 'admin', CURRENT_DATE, NULL, NULL, true, 'dragan.novkovic@rs.ey.com', 'Dragan', 'Novkovic', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'dnovkovic', 1),
    (5, 'admin', CURRENT_DATE, NULL, NULL, true, 'nina.udovic@rs.ey.com', 'Nina', 'Udovic', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'nudovic', 1),
    (6, 'client admin', CURRENT_DATE, NULL, NULL, true, 'pera.peric@imlek.rs', 'Pera', 'Peric', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'pperic', 2),
    (7, 'user', CURRENT_DATE, NULL, NULL, true, 'zika.zikic@imlek.rs', 'Zika', 'Zikic', '$2y$10$cvzNVM5pnjmBc1w.sDE43.4k4rhS8EIhlHz9fzJkdp0SSJXCaOOqu', 'zzikic', 2)
;

# User with roles
INSERT INTO
    user_role (user_id, role_id)
VALUES
    (1, 1),
    (2, 1),
    (3, 1),
    (4, 1),
    (5, 1),
    (6, 2),
    (7, 3)
;
